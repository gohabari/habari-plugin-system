package main

import (
	"net/http"
)

type Plugin interface {
	Configure(bs []byte) error
	GenerateConfiguration() []byte
	Handle(s string, r *http.Request) (string, int, error)
	Type() string
}
